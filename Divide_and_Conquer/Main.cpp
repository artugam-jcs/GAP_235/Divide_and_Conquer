#include <iostream>

template<typename T>
T BinarySearch(T arr[], T target, size_t low, size_t high)
{
	size_t size = (low + high);
	size_t middle = size / 2;

	T val = arr[middle];

	if (val == target)
		return (T)middle;

	if (target < arr[low] || target > arr[high])
		return -1;

	if (val > target)
		return BinarySearch(arr, target, low, middle - 1);

	if (val < target)
		return BinarySearch(arr, target, middle + 1, high);

	return -1;
}

void Test(int arr[], size_t size, int target)
{
	int result = BinarySearch(arr, target, 0, size - 1);

	if (result == -1)
	{
		std::cout << "Search not found" << std::endl;
		return;
	}

	std::cout << "Index: " << result << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

void Test1()
{
	const size_t size = 11;
	int arr[size]{ 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, };
	Test(arr, size, 50);
}

void Test2()
{
	const size_t size = 2;
	int arr[size]{ 0, 10, };
	Test(arr, size, 0);
}

/**
 * @brief Program Entry
 */
int main()
{
	Test1();
	Test2();

	return 0;
}
